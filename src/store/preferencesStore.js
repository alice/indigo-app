import createStore from 'indigo/fiber-store'
import constants from '../constants.js'


const preferencesStore = createStore({

    state: {
        themeName: constants.defaultThemeName
    },

    mutations: {
        setTheme(state, themeName) {
            state.themeName = themeName
        }
    }
})


export default preferencesStore
