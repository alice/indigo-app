import createStore from 'indigo/fiber-store'
import Indigo from 'indigo/api-client'
import constants from '../constants.js'


// indigo API client instance

const indigo = new Indigo(constants.indigoEndpoint)


const medicalDiagnosisStore = createStore({

    state: {
        medicalConditionDistribution: null,
        medicalConditionNames: null,
        modalMedicalCondition: null,
    },

    mutations: {

        setMedicalConditionDistribution(state, distribution) {
            state.medicalConditionDistribution = distribution
        },

        setMedicalConditionNames(state, medicalConditionNames) {
            state.medicalConditionNames = medicalConditionNames
        },

        setModalMedicalCondition(state, modalMedicalCondition) {
            state.modalMedicalCondition = modalMedicalCondition
        }
    },

    actions: {

        // set the initial medical condition distribution.

        initMedicalConditionDistribution({ commit }) {

            indigo
                .getPrior()
                .then(({ medicalConditionNames, priorDistribution }) => {

                    commit('setMedicalConditionDistribution', priorDistribution)
                    commit('setMedicalConditionNames', medicalConditionNames)
                })
        },


        // change the medial condition distribution based on symptoms stated by
        // patient.

        updateMedicalConditionDistribution({ commit, state }, symptomsMap) {

            const priorDistribution = state.medicalConditionDistribution

            indigo
                .postPosterior(priorDistribution, symptomsMap)
                .then(({ posteriorDistribution, modalMedicalCondition }) => {

                    commit('setMedicalConditionDistribution', posteriorDistribution)
                    commit('setModalMedicalCondition', modalMedicalCondition)
                })
        }
    }
})


export default medicalDiagnosisStore
