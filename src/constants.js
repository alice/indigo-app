const indigoEndpoint = 'http://localhost:5000/'
const defaultThemeName = 'dark'
const appElement = '#app'


export default {
    indigoEndpoint,
    defaultThemeName,
    appElement
}
