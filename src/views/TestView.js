// TestView, a page for testing stuff out

import medicalDiagnosisStore from '../store/medicalDiagnosisStore.js'

export default function TestView() {

    return 'this is a test view: ' + medicalDiagnosisStore.state
}
