import constants from './constants.js'
import App from './components/App.js'


window.onload = function() {

    document.querySelector(constants.appElement)
        .append(App())
}
