# Install @indigo dependencies in node_modules/. This script is required until
# those dependencies have been published to the npm registry.

from os import system

system('rm -rf ./node_modules/indigo/')
system('mkdir ./node_modules/indigo/')

system('git clone git@codeberg.org:alice/indigo-fibrous.git ./node_modules/indigo/fibrous')
system('git clone git@codeberg.org:alice/indigo-fiber.git ./node_modules/indigo/fiber')
system('git clone git@codeberg.org:alice/indigo-fiber-store.git ./node_modules/indigo/fiber-store')
system('git clone git@codeberg.org:alice/indigo-fiber-navigation.git ./node_modules/indigo/fiber-navigation')
system('git clone git@codeberg.org:alice/indigo-fiber-declarative-elements.git ./node_modules/indigo/fiber-declarative-elements')
system('git clone git@codeberg.org:alice/indigo-api-client.git ./node_modules/indigo/api-client')
